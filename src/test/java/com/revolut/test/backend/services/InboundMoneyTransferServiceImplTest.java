package com.revolut.test.backend.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import com.revolut.test.backend.datastore.DummyDataStore;
import com.revolut.test.backend.model.Account;
import com.revolut.test.backend.constants.MoneyTransferEventCode;
import com.revolut.test.backend.model.MoneyTransferRequest;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class InboundMoneyTransferServiceImplTest {

  @Mock private DummyDataStore mockDummyDataStore;

  @InjectMocks private InboundMoneyTransferServiceImpl moneyTransferService;

  private static final String TEST_SENDER_ACCOUNT_ID = "TEST_SENDER_ID";
  private static final String TEST_RECEIVER_ACCOUNT_ID = "TEST_RECEIVER_ID";
  private static final BigDecimal TEST_MONEY_TRANSFER_AMOUNT = BigDecimal.valueOf(10);
  private static final String TEST_GBP_CURRENCY_CODE = "GBP";
  private static final String TEST_EURO_CURRENCY_CODE = "EUR";

  private static final MoneyTransferRequest TEST_GBP_TRANSFER_REQUEST =
      MoneyTransferRequest.builder()
          .senderAccountId(TEST_SENDER_ACCOUNT_ID)
          .receiverAccountId(TEST_RECEIVER_ACCOUNT_ID)
          .currencyCode(TEST_GBP_CURRENCY_CODE)
          .transactionAmount(TEST_MONEY_TRANSFER_AMOUNT)
          .build();

  private static final Account TEST_GBP_EXAMPLE_SENDER_ACCOUNT =
      Account.builder()
          .accountId(TEST_SENDER_ACCOUNT_ID)
          .balance(BigDecimal.valueOf(200))
          .currencyCode(TEST_GBP_CURRENCY_CODE)
          .build();

  @Test
  public void shouldReturnInvalidRequestAmountErrorWhenRequestAmountNonPositive() {
    MoneyTransferRequest transferRequestWithNegativeAmount =
        MoneyTransferRequest.builder()
            .senderAccountId(TEST_SENDER_ACCOUNT_ID)
            .receiverAccountId(TEST_RECEIVER_ACCOUNT_ID)
            .currencyCode(TEST_GBP_CURRENCY_CODE)
            .transactionAmount(BigDecimal.valueOf(-1))
            .build();

    assertEquals(
        MoneyTransferEventCode.REQUEST_AMOUNT_INVALID,
        moneyTransferService.transfer(transferRequestWithNegativeAmount));
  }

  @Test
  public void shouldReturnInvalidSenderAccountWhenUnableToFindSenderInDB() {
    when(mockDummyDataStore.getAccountWithId(TEST_SENDER_ACCOUNT_ID)).thenReturn(Optional.empty());
    assertEquals(
        MoneyTransferEventCode.SENDER_ACCOUNT_INVALID,
        moneyTransferService.transfer(TEST_GBP_TRANSFER_REQUEST));
  }

  @Test
  public void
      shouldReturnSenderCurrencyCodeErrorWhenSenderCurrencyCodeAndRequestCurrencyDoesNotMatch() {
    Account euroSenderAccount =
        Account.builder()
            .accountId(TEST_SENDER_ACCOUNT_ID)
            .currencyCode(TEST_EURO_CURRENCY_CODE)
            .build();
    when(mockDummyDataStore.getAccountWithId(TEST_SENDER_ACCOUNT_ID))
        .thenReturn(Optional.of(euroSenderAccount));
    assertEquals(
        MoneyTransferEventCode.SENDER_CURRENCY_CODE_ERROR,
        moneyTransferService.transfer(TEST_GBP_TRANSFER_REQUEST));
  }

  @Test
  public void shouldReturnSenderAccountBalanceErrorWhenSenderBalanceIsLowerThanRequestedAmount() {
    Account gbpSenderAccount =
        Account.builder()
            .accountId(TEST_SENDER_ACCOUNT_ID)
            .balance(BigDecimal.valueOf(3))
            .currencyCode(TEST_GBP_CURRENCY_CODE)
            .build();
    when(mockDummyDataStore.getAccountWithId(TEST_SENDER_ACCOUNT_ID))
        .thenReturn(Optional.of(gbpSenderAccount));
    assertEquals(
        MoneyTransferEventCode.SENDER_ACCOUNT_BALANCE_ERROR,
        moneyTransferService.transfer(TEST_GBP_TRANSFER_REQUEST));
  }

  @Test
  public void shouldReturnInvalidReceiverAccountWhenUnableToFindReceiverInDB() {
    when(mockDummyDataStore.getAccountWithId(TEST_SENDER_ACCOUNT_ID))
        .thenReturn(Optional.of(TEST_GBP_EXAMPLE_SENDER_ACCOUNT));
    when(mockDummyDataStore.getAccountWithId(TEST_RECEIVER_ACCOUNT_ID))
        .thenReturn(Optional.empty());

    assertEquals(
        MoneyTransferEventCode.RECEIVER_ACCOUNT_INVALID,
        moneyTransferService.transfer(TEST_GBP_TRANSFER_REQUEST));
  }

  @Test
  public void
      shouldReturnInvalidReceiverCurrencyCodeWhenReceiverCurrencyCodeAndRequestCurrencyCodeDoesNotMatch() {
    Account gbpReceiver =
        Account.builder()
            .accountId(TEST_RECEIVER_ACCOUNT_ID)
            .currencyCode(TEST_EURO_CURRENCY_CODE)
            .build();
    when(mockDummyDataStore.getAccountWithId(TEST_SENDER_ACCOUNT_ID))
        .thenReturn(Optional.of(TEST_GBP_EXAMPLE_SENDER_ACCOUNT));
    when(mockDummyDataStore.getAccountWithId(TEST_RECEIVER_ACCOUNT_ID))
        .thenReturn(Optional.of(gbpReceiver));

    assertEquals(
        MoneyTransferEventCode.RECEIVER_CURRENCY_CODE_ERROR,
        moneyTransferService.transfer(TEST_GBP_TRANSFER_REQUEST));
  }

  @Test
  public void shouldSuccessfullyTransferMoney() {
    Account gbpSender =
        Account.builder()
            .accountId(TEST_SENDER_ACCOUNT_ID)
            .balance(BigDecimal.valueOf(100))
            .currencyCode(TEST_GBP_CURRENCY_CODE)
            .build();
    Account gbpReceiver =
        Account.builder()
            .accountId(TEST_RECEIVER_ACCOUNT_ID)
            .balance(BigDecimal.valueOf(2))
            .currencyCode(TEST_GBP_CURRENCY_CODE)
            .build();

    when(mockDummyDataStore.getAccountWithId(TEST_SENDER_ACCOUNT_ID))
        .thenReturn(Optional.of(gbpSender));
    when(mockDummyDataStore.getAccountWithId(TEST_RECEIVER_ACCOUNT_ID))
        .thenReturn(Optional.of(gbpReceiver));

    assertEquals(
        MoneyTransferEventCode.TRANSFER_SUCCESS,
        moneyTransferService.transfer(TEST_GBP_TRANSFER_REQUEST));
    assertEquals(
        BigDecimal.valueOf(100).subtract(TEST_MONEY_TRANSFER_AMOUNT), gbpSender.getBalance());
    assertEquals(BigDecimal.valueOf(2).add(TEST_MONEY_TRANSFER_AMOUNT), gbpReceiver.getBalance());
  }

  @Test
  public void shouldNotRunIntoRaceConditionWhenMultipleRequestsFiredConcurrently()
      throws InterruptedException {
    Account gbpSender =
        Account.builder()
            .accountId(TEST_SENDER_ACCOUNT_ID)
            .balance(BigDecimal.valueOf(10000))
            .currencyCode(TEST_GBP_CURRENCY_CODE)
            .build();
    Account gbpReceiver =
        Account.builder()
            .accountId(TEST_RECEIVER_ACCOUNT_ID)
            .balance(BigDecimal.valueOf(2))
            .currencyCode(TEST_GBP_CURRENCY_CODE)
            .build();

    when(mockDummyDataStore.getAccountWithId(TEST_SENDER_ACCOUNT_ID))
        .thenReturn(Optional.of(gbpSender));
    when(mockDummyDataStore.getAccountWithId(TEST_RECEIVER_ACCOUNT_ID))
        .thenReturn(Optional.of(gbpReceiver));

    ExecutorService service = Executors.newFixedThreadPool(3);
    IntStream.range(0, 100)
        .parallel()
        .forEach(
            count ->
                service.submit(
                    () -> {
                      moneyTransferService.transfer(TEST_GBP_TRANSFER_REQUEST);
                    }));
    service.awaitTermination(1000, TimeUnit.MILLISECONDS);

    Assert.assertEquals(
        BigDecimal.valueOf(10000)
            .subtract(TEST_MONEY_TRANSFER_AMOUNT.multiply(BigDecimal.valueOf(100))),
        mockDummyDataStore.getAccountWithId(TEST_SENDER_ACCOUNT_ID).get().getBalance());
    Assert.assertEquals(
        BigDecimal.valueOf(2).add(TEST_MONEY_TRANSFER_AMOUNT.multiply(BigDecimal.valueOf(100))),
        mockDummyDataStore.getAccountWithId(TEST_RECEIVER_ACCOUNT_ID).get().getBalance());
  }
}
