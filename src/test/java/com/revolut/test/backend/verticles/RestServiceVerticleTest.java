package com.revolut.test.backend.verticles;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.test.backend.MoneyTransferModule;
import com.revolut.test.backend.constants.MoneyTransferEventCode;
import com.revolut.test.backend.model.MoneyTransferRequest;
import com.revolut.test.backend.model.MoneyTransferResponse;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.ServerSocket;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(VertxUnitRunner.class)
public class RestServiceVerticleTest {
  private Vertx vertx;

  private Integer testPort;

  private static final String TEST_VALID_SENDER_ACCOUNT_ID = "12345678";
  private static final String TEST_INVALID_SENDER_ACCOUNT_ID = "INVALID_ID";
  private static final String TEST_VALID_RECEIVER_ACCOUNT_ID = "87654321";
  private static final String TEST_VALID_RECEIVER_ACCOUNT_ID2 = "98765432";
  private static final String TEST_INVALID_RECEIVER_ACCOUNT_ID = "INVALID_ID";

  private static final String CURRENCY_CODE_GBP = "GBP";
  private static final String CURRENCY_CODE_EUR = "EUR";

  @Before
  public void setUp(TestContext testContext) throws IOException {
    vertx = Vertx.vertx();
    ServerSocket socket = new ServerSocket(0);
    testPort = socket.getLocalPort();
    socket.close();

    DeploymentOptions deploymentOptions =
        new DeploymentOptions().setConfig(new JsonObject().put("http.port", testPort));

    Injector injector = Guice.createInjector(new MoneyTransferModule());
    RestServiceVerticle restServiceVerticle = injector.getInstance(RestServiceVerticle.class);
    vertx.deployVerticle(restServiceVerticle, deploymentOptions, testContext.asyncAssertSuccess());
  }

  @After
  public void tearDown(TestContext testContext) {
    vertx.close(testContext.asyncAssertSuccess());
  }

  @Test
  public void shouldTransferMoneySuccessfullyWithValidRequestAndAccount(TestContext testContext) {
    MoneyTransferRequest validRequest =
        MoneyTransferRequest.builder()
            .senderAccountId(TEST_VALID_SENDER_ACCOUNT_ID)
            .receiverAccountId(TEST_VALID_RECEIVER_ACCOUNT_ID)
            .currencyCode(CURRENCY_CODE_GBP)
            .transactionAmount(BigDecimal.valueOf(20.1))
            .build();
    final String requestJson = Json.encodePrettily(validRequest);
    testMoneyTransfer(testContext, requestJson, 200, MoneyTransferEventCode.TRANSFER_SUCCESS);
  }

  @Test
  public void shouldReturnErrorWhenRequestAmountIsNegative(TestContext testContext) {
    MoneyTransferRequest invalidRequest =
        MoneyTransferRequest.builder()
            .senderAccountId(TEST_VALID_SENDER_ACCOUNT_ID)
            .receiverAccountId(TEST_VALID_RECEIVER_ACCOUNT_ID)
            .currencyCode(CURRENCY_CODE_GBP)
            .transactionAmount(BigDecimal.valueOf(-1))
            .build();
    final String requestJson = Json.encodePrettily(invalidRequest);
    testMoneyTransfer(testContext, requestJson, 400, MoneyTransferEventCode.REQUEST_AMOUNT_INVALID);
  }

  @Test
  public void shouldReturnErrorWhenSenderAccountNotExistInDB(TestContext testContext) {
    MoneyTransferRequest invalidRequest =
        MoneyTransferRequest.builder()
            .senderAccountId(TEST_INVALID_SENDER_ACCOUNT_ID)
            .receiverAccountId(TEST_VALID_RECEIVER_ACCOUNT_ID)
            .currencyCode(CURRENCY_CODE_GBP)
            .transactionAmount(BigDecimal.valueOf(20.1))
            .build();

    final String requestJson = Json.encodePrettily(invalidRequest);
    testMoneyTransfer(testContext, requestJson, 400, MoneyTransferEventCode.SENDER_ACCOUNT_INVALID);
  }

  @Test
  public void shouldReturnErrorWhenSenderCurrencyDoesNotMatchRequest(TestContext testContext) {
    MoneyTransferRequest invalidRequest =
        MoneyTransferRequest.builder()
            .senderAccountId(TEST_VALID_SENDER_ACCOUNT_ID)
            .receiverAccountId(TEST_VALID_RECEIVER_ACCOUNT_ID)
            .currencyCode(CURRENCY_CODE_EUR)
            .transactionAmount(BigDecimal.valueOf(20.1))
            .build();

    final String requestJson = Json.encodePrettily(invalidRequest);
    testMoneyTransfer(
        testContext, requestJson, 400, MoneyTransferEventCode.SENDER_CURRENCY_CODE_ERROR);
  }

  @Test
  public void shouldReturnErrorWhenRequestAmountMoreThanSenderBalance(TestContext testContext) {
    MoneyTransferRequest invalidRequest =
        MoneyTransferRequest.builder()
            .senderAccountId(TEST_VALID_SENDER_ACCOUNT_ID)
            .receiverAccountId(TEST_VALID_RECEIVER_ACCOUNT_ID)
            .currencyCode(CURRENCY_CODE_GBP)
            .transactionAmount(BigDecimal.valueOf(200000))
            .build();

    final String requestJson = Json.encodePrettily(invalidRequest);
    testMoneyTransfer(
        testContext, requestJson, 400, MoneyTransferEventCode.SENDER_ACCOUNT_BALANCE_ERROR);
  }

  @Test
  public void shouldReturnErrorWhenReceiverAccountNotValid(TestContext testContext) {
    MoneyTransferRequest invalidRequest =
        MoneyTransferRequest.builder()
            .senderAccountId(TEST_VALID_SENDER_ACCOUNT_ID)
            .receiverAccountId(TEST_INVALID_RECEIVER_ACCOUNT_ID)
            .currencyCode(CURRENCY_CODE_GBP)
            .transactionAmount(BigDecimal.valueOf(20))
            .build();

    final String requestJson = Json.encodePrettily(invalidRequest);
    testMoneyTransfer(
        testContext, requestJson, 400, MoneyTransferEventCode.RECEIVER_ACCOUNT_INVALID);
  }

  @Test
  public void shouldReturnErrorWhenReceiverAccountCurrencyCodeDoesNotMatchRequest(
      TestContext testContext) {
    MoneyTransferRequest invalidRequest =
        MoneyTransferRequest.builder()
            .senderAccountId(TEST_VALID_SENDER_ACCOUNT_ID)
            .receiverAccountId(TEST_VALID_RECEIVER_ACCOUNT_ID2)
            .currencyCode(CURRENCY_CODE_GBP)
            .transactionAmount(BigDecimal.valueOf(20))
            .build();

    final String requestJson = Json.encodePrettily(invalidRequest);
    testMoneyTransfer(
        testContext, requestJson, 400, MoneyTransferEventCode.RECEIVER_CURRENCY_CODE_ERROR);
  }

  private void testMoneyTransfer(
      final TestContext testContext,
      final String requestJson,
      final int expectedStatusCode,
      final MoneyTransferEventCode expectedTransferEventCode) {
    final Async async = testContext.async();

    vertx
        .createHttpClient()
        .post(testPort, "localhost", "/transfer")
        .putHeader("content-type", "application/json")
        .putHeader("content-length", Integer.toString(requestJson.length()))
        .handler(
            response -> {
              testContext.assertEquals(expectedStatusCode, response.statusCode());
              response.bodyHandler(
                  responseBody -> {
                    final MoneyTransferResponse moneyTransferResponse =
                        Json.decodeValue(responseBody.toString(), MoneyTransferResponse.class);
                    testContext.assertEquals(
                        expectedTransferEventCode.getCode(), moneyTransferResponse.getCode());
                    testContext.assertEquals(
                        expectedTransferEventCode.getMessage(), moneyTransferResponse.getMessage());
                    async.complete();
                  });
            })
        .write(requestJson)
        .end();
  }
}
