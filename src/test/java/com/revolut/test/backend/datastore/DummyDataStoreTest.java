package com.revolut.test.backend.datastore;

import static org.junit.Assert.*;

import com.revolut.test.backend.model.Account;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DummyDataStoreTest {

  private DummyDataStore dummyDataStore;

  @Before
  public void setup() {
    this.dummyDataStore = new DummyDataStore();
  }

  @Test
  public void shouldLoadDataSuccessfullyWhenInstantiateIt() {
    assertNotNull(dummyDataStore);
    Assert.assertEquals(
        BigDecimal.valueOf(200.12), dummyDataStore.getAccountWithId("12345678").get().getBalance());
  }

  @Test
  public void shouldUpdateAccountInfoSuccessfully() {
    Account account = this.dummyDataStore.getAccountWithId("12345678").get();
    account.setBalance(account.getBalance().subtract(BigDecimal.valueOf(10)));
    dummyDataStore.updateAccountInfo("123456789", account);
    Assert.assertEquals(
        BigDecimal.valueOf(190.12), dummyDataStore.getAccountWithId("12345678").get().getBalance());
  }
}
