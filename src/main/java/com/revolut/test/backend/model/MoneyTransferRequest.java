package com.revolut.test.backend.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MoneyTransferRequest {
  @JsonProperty(value = "sender_id")
  private String senderAccountId;

  @JsonProperty(value = "receiver_id")
  private String receiverAccountId;

  @JsonProperty(value = "currency_code")
  private String currencyCode;

  @JsonProperty(value = "transaction_amount")
  private BigDecimal transactionAmount;
}
