package com.revolut.test.backend;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.revolut.test.backend.datastore.DummyDataStore;
import com.revolut.test.backend.services.InboundMoneyTransferServiceImpl;
import com.revolut.test.backend.services.MoneyTransferService;

public class MoneyTransferModule extends AbstractModule {
  @Override
  protected void configure() {
    bind(DummyDataStore.class).asEagerSingleton();
  }

  @Provides
  @Named("Inbound Money Transfer Service")
  @Singleton
  private MoneyTransferService provideInboundMoneyTransferService(DummyDataStore dummyDataStore) {
    return new InboundMoneyTransferServiceImpl(dummyDataStore);
  }
}
