package com.revolut.test.backend.datastore;

import com.revolut.test.backend.model.Account;

import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.java.Log;

@Log
public class DummyDataStore {
  private static final String DB_FILENAME = "accounts.json";
  private static final String ACCOUNTS_ARRAY_KEY = "accounts";

  //  Not thread-safe. Could have used ConcurrentHashMap but decided to have a bit of fun :)
  private HashMap<String, Account> fakeDB;

  public DummyDataStore() {
    loadData();
  }

  private void loadData() {
    try {
      String fileContents =
          String.join("", Files.readAllLines(getFileFromResource(DB_FILENAME).toPath()));
      Map<String, Account> dataMap =
          new JsonObject(fileContents)
              .getJsonArray(ACCOUNTS_ARRAY_KEY)
              .stream()
              .map(o -> Json.decodeValue(o.toString(), Account.class))
              .collect(
                  Collectors.toMap(
                      Account::getAccountId, Function.identity(), (a, b) -> a, HashMap::new));

      this.fakeDB = new HashMap<>(dataMap);
      log.info("Dummy Account Data successfully loaded");
    } catch (IOException e) {
      this.fakeDB = new HashMap<>();
      log.warning("Failed to load dummy data, fakeDB is empty");
    }
  }

  private File getFileFromResource(final String dbFilename) {
    URL resource = DummyDataStore.class.getClassLoader().getResource(dbFilename);
    if (resource == null) {
      throw new IllegalArgumentException(dbFilename + " not found");
    } else {
      return new File(resource.getFile());
    }
  }

  public Optional<Account> getAccountWithId(String accountId) {
    return Optional.ofNullable(this.fakeDB.get(accountId));
  }

  public void updateAccountInfo(String accountId, Account updatedAccountInfo) {
    this.fakeDB.put(accountId, updatedAccountInfo);
  }
}
