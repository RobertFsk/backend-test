package com.revolut.test.backend.services;

import com.revolut.test.backend.constants.MoneyTransferEventCode;
import com.revolut.test.backend.model.MoneyTransferRequest;

public interface MoneyTransferService {
  MoneyTransferEventCode transfer(MoneyTransferRequest moneyTransferRequest);
}
