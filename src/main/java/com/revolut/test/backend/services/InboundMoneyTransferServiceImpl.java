package com.revolut.test.backend.services;

import com.revolut.test.backend.datastore.DummyDataStore;
import com.revolut.test.backend.model.Account;
import com.revolut.test.backend.constants.MoneyTransferEventCode;
import com.revolut.test.backend.model.MoneyTransferRequest;
import java.math.BigDecimal;
import java.util.Optional;
import javax.inject.Inject;

public class InboundMoneyTransferServiceImpl implements MoneyTransferService {

  private DummyDataStore dummyDataStore;

  @Inject
  public InboundMoneyTransferServiceImpl(DummyDataStore dummyDataStore) {
    this.dummyDataStore = dummyDataStore;
  }

  @Override
  public MoneyTransferEventCode transfer(final MoneyTransferRequest moneyTransferRequest) {

    if (moneyTransferRequest.getTransactionAmount().compareTo(BigDecimal.ZERO) <= 0) {
      return MoneyTransferEventCode.REQUEST_AMOUNT_INVALID;
    }

    //    The instance of this verticle is set to be 1 by default in Vert.x, so it's ok to use
    // synchronized block here
    synchronized (this) {
      Optional<Account> senderAccount =
          dummyDataStore.getAccountWithId(moneyTransferRequest.getSenderAccountId());
      Optional<Account> receiverAccount =
          dummyDataStore.getAccountWithId(moneyTransferRequest.getReceiverAccountId());

      Optional<MoneyTransferEventCode> senderAccountInvalidResult =
          validateSenderAccount(moneyTransferRequest, senderAccount);
      if (senderAccountInvalidResult.isPresent()) {
        return senderAccountInvalidResult.get();
      }

      Optional<MoneyTransferEventCode> receiverAccountInvalidResult =
          validateReceiverAccount(moneyTransferRequest, receiverAccount);
      if (receiverAccountInvalidResult.isPresent()) {
        return receiverAccountInvalidResult.get();
      }

      makeTransaction(moneyTransferRequest, senderAccount, receiverAccount);
    }
    return MoneyTransferEventCode.TRANSFER_SUCCESS;
  }

  private void makeTransaction(
      final MoneyTransferRequest moneyTransferRequest,
      final Optional<Account> senderAccount,
      final Optional<Account> receiverAccount) {
    senderAccount
        .get()
        .setBalance(
            senderAccount.get().getBalance().subtract(moneyTransferRequest.getTransactionAmount()));
    receiverAccount
        .get()
        .setBalance(
            receiverAccount.get().getBalance().add(moneyTransferRequest.getTransactionAmount()));
    dummyDataStore.updateAccountInfo(senderAccount.get().getAccountId(), senderAccount.get());
    dummyDataStore.updateAccountInfo(receiverAccount.get().getAccountId(), receiverAccount.get());
  }

  private Optional<MoneyTransferEventCode> validateReceiverAccount(
      final MoneyTransferRequest moneyTransferRequest, final Optional<Account> receiverAccount) {
    if (receiverAccount.isPresent()) {
      if (!receiverAccount.get().getCurrencyCode().equals(moneyTransferRequest.getCurrencyCode())) {
        return Optional.of(MoneyTransferEventCode.RECEIVER_CURRENCY_CODE_ERROR);
      }
    } else {
      return Optional.of(MoneyTransferEventCode.RECEIVER_ACCOUNT_INVALID);
    }
    return Optional.empty();
  }

  private Optional<MoneyTransferEventCode> validateSenderAccount(
      final MoneyTransferRequest moneyTransferRequest, final Optional<Account> senderAccount) {
    if (senderAccount.isPresent()) {
      if (!senderAccount.get().getCurrencyCode().equals(moneyTransferRequest.getCurrencyCode())) {
        return Optional.of(MoneyTransferEventCode.SENDER_CURRENCY_CODE_ERROR);
      }
      if (senderAccount.get().getBalance().compareTo(moneyTransferRequest.getTransactionAmount())
          < 0) {
        return Optional.of(MoneyTransferEventCode.SENDER_ACCOUNT_BALANCE_ERROR);
      }
    } else {
      return Optional.of(MoneyTransferEventCode.SENDER_ACCOUNT_INVALID);
    }
    return Optional.empty();
  }
}
