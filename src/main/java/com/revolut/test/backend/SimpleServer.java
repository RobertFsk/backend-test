package com.revolut.test.backend;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.revolut.test.backend.verticles.RestServiceVerticle;

import io.vertx.core.Vertx;
import lombok.extern.java.Log;

@Log
public class SimpleServer {

  public static void main(String[] args) {
    Injector injector = Guice.createInjector(new MoneyTransferModule());
    RestServiceVerticle restServiceVerticle = injector.getInstance(RestServiceVerticle.class);
    Vertx.vertx().deployVerticle(restServiceVerticle);
    log.info("Started a simple Http Server, listening at port 8080");
  }
}
