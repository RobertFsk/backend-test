package com.revolut.test.backend.constants;

public enum MoneyTransferEventCode {
  TRANSFER_SUCCESS("2000", "Transfer Completed"),
  REQUEST_AMOUNT_INVALID("4001", "Invalid Transaction Amount"),
  SENDER_ACCOUNT_INVALID("4002", "Invalid Sender Account"),
  SENDER_CURRENCY_CODE_ERROR(
      "4003", "Sender Account Currency Code Different from Currency Code in Request"),
  SENDER_ACCOUNT_BALANCE_ERROR("4004", "Sender Account Balance Not Enough"),

  RECEIVER_ACCOUNT_INVALID("4005", "Invalid Receiver Account"),
  RECEIVER_CURRENCY_CODE_ERROR(
      "4006", "Receiver Account Currency Code Different from Currency Code in Request");

  private final String code;
  private final String message;

  MoneyTransferEventCode(String code, String message) {
    this.code = code;
    this.message = message;
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
