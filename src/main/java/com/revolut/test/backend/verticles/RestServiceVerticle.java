package com.revolut.test.backend.verticles;

import com.google.inject.Inject;
import com.google.inject.name.Named;
import com.revolut.test.backend.constants.MoneyTransferEventCode;
import com.revolut.test.backend.model.MoneyTransferRequest;
import com.revolut.test.backend.model.MoneyTransferResponse;
import com.revolut.test.backend.services.MoneyTransferService;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import lombok.extern.java.Log;

@Log
public class RestServiceVerticle extends AbstractVerticle {
  private MoneyTransferService moneyTransferService;

  @Inject
  public RestServiceVerticle(
      @Named("Inbound Money Transfer Service") MoneyTransferService moneyTransferService) {
    this.moneyTransferService = moneyTransferService;
  }

  @Override
  public void start() {
    Router router = Router.router(vertx);
    router.route().handler(BodyHandler.create());

    router.post("/transfer").handler(this::handleMoneyTransfer);

    vertx.createHttpServer().requestHandler(router).listen(config().getInteger("http.port", 8080));
  }

  private void handleMoneyTransfer(RoutingContext routingContext) {

    HttpServerResponse response = routingContext.response();
    MoneyTransferRequest moneyTransferRequest =
        Json.decodeValue(routingContext.getBodyAsString(), MoneyTransferRequest.class);

    MoneyTransferEventCode transferResult =
        this.moneyTransferService.transfer(moneyTransferRequest);

    response.putHeader("content-type", "application/json");

    if (transferResult == MoneyTransferEventCode.TRANSFER_SUCCESS) {
      response.setStatusCode(200);
    } else {
      response.setStatusCode(400);
    }

    response.end(
        Json.encode(
            MoneyTransferResponse.builder()
                .code(transferResult.getCode())
                .message(transferResult.getMessage())
                .build()));
  }
}
